from tkinter import *
from tkinter.filedialog import askdirectory
import glob
from PIL import Image,ImageTk
import os
import shutil
import threading


tk = Tk()

tk.columnconfigure(0, weight=1)
tk.rowconfigure(0, weight=1)

frame = Frame(tk) #width=600,height=600
frame.grid(row=0,column=0,sticky="NSEW")
frame.rowconfigure(0,weight=1)
frame.rowconfigure(1,weight=0)
frame.rowconfigure(2,weight=0)
frame.columnconfigure(0,weight=1)
frame.columnconfigure(1,weight=1)
frame.columnconfigure(2,weight=1)
frame.columnconfigure(3,weight=1)

canvas = Canvas(frame,width=200,height=200)
canvas.grid(row=0,column=0,columnspan=4,sticky="NSEW")

my_directory=""
my_files=[""]
pic_position = 0
label1_string = StringVar()
c_x,c_y = IntVar(),IntVar()
temp_x,temp_y = IntVar(),IntVar()
c_x.set(200)
c_y.set(200)


def is_resizing():
	global c_x
	global c_y
	global temp_x
	global temp_y
	global my_files
	global pic_position
	print(temp_x.get(),temp_y.get(),c_x.get(),c_y.get())
	if c_x.get() != temp_x.get() or c_y.get != temp_y.get():
		changeCanvasPict(my_files[pic_position])
		temp_x.set(c_x.get())
		temp_y.set(c_y.get)()
	else:
		changeCanvasPict(my_files[pic_position])


t = threading.Timer(0.2, is_resizing)
t.start()


def resize(event):
	global my_files
	global pic_position
	global c_x
	global c_y
	#frame.width,frame.height = event.width,event.height-50
	c_x.set(event.width)
	c_y.set(event.height)
	#changeCanvasPict(my_files[pic_position])
	print(event.width,event.height)

def resizeToBounds(px,py,bx,by):
	ratio = 1
	if px > bx or py > by:
		ratio = min([bx/px,by/py])
	return (int(px*ratio),int(py*ratio))

def getFiles():
	global my_directory
	global my_files
	global pic_position
	my_directory = askdirectory(parent=tk,initialdir="~/Dropbox/Obrazky",title="Please select a directory")
	print(my_directory)
	my_files = glob.glob(my_directory + "/*.*g")
	print(my_files)
	print(os.path.expanduser("~"))
	changeCanvasPict(my_files[pic_position])


def changeCanvasPict(pict_file):
	global label1_string
	global c_x
	global c_y
	im = Image.open(pict_file)
	(w,h) = im.size
	#print(c_size)
	im = im.resize(resizeToBounds(w,h,c_x.get(),c_y.get()),Image.LANCZOS)
	canvas.my_image = ImageTk.PhotoImage(im)
	canvas.create_image(0+int(c_x.get()/2),0+int(c_y.get()/2),anchor=CENTER,image=canvas.my_image)
	label1_string.set(os.path.basename(pict_file))
	tk.update()

def incPicPos():
	global pic_position
	global my_files
	pic_position += 1
	if pic_position > len(my_files) - 1:
		pic_position = 0
	changeCanvasPict(my_files[pic_position])

def decPicPos():
	global pic_position
	global my_files
	pic_position -= 1
	if pic_position < 0:
		pic_position = len(my_files) - 1
	changeCanvasPict(my_files[pic_position])

def movePic(pos):
	global my_directory
	global my_files
	print(my_directory + "/pw_deleted")
	if not os.path.exists(my_directory + "/pw_deleted"):
		os.makedirs(my_directory + "/pw_deleted")
		print("dir creted")

	print(os.path.basename(my_files[pos]))
	dest = my_directory + "/pw_deleted/" + os.path.basename(my_files[pos])
	print(dest)
	shutil.move(my_files[pos],dest)
	print("deleting file @" + my_files[pos])
	del my_files[pos]
	changeCanvasPict(my_files[pos])

def left_k(event):
	print("left")
	decPicPos()

def right_k(event):
	print("right")
	incPicPos()

def open_k(event):
	#print("pressed", repr(event.char))
	getFiles()
def del_k(event):
	movePic()

btn = Button(frame,text="open",command=getFiles,anchor=SW)
btn.grid(row=2,column=0,sticky="W")

btn_back = Button(frame,text="<-",command=decPicPos,anchor=SW)
btn_back.grid(row=2,column=1,sticky="E")
btn_next = Button(frame,text="->",command=incPicPos,anchor=SW)
btn_next.grid(row=2,column=2,sticky="W")
btn_del = Button(frame,text="delete",command=lambda: movePic(pic_position),anchor=SW)
btn_del.grid(row=2,column=3,sticky="E")

tk.bind("<Left>",left_k)
tk.bind("<Right>",right_k)
tk.bind("o",open_k)
tk.bind("d",del_k)


label1 = Label(frame,textvariable=label1_string)
label1.grid(row=1,column=0,columnspan=4,sticky="W")

canvas.bind("<Configure>",resize)

# changeCanvasPict(tk,os.path.expanduser("~") + "/Dropbox/Obrazky/283004508903047168_35s_dx.jpg")
tk.mainloop()
